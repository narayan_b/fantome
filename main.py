from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector
import pymysql

mydb = mysql.connector.Connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='fantome'
)

def get_page_content(url):
    rep = urlopen("https://www.paranormaldatabase.com"+url)
    return rep.read(), rep.getcode()


contenu_de_index, code = get_page_content('/index.html')
soup = BeautifulSoup(contenu_de_index, features="html.parser")
home_link_categorie = []

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    home_link_categorie.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    home_link_categorie.append(url)

liste_quarter = soup.find_all(class_="w3-quarter")
for third in liste_quarter:
    lien = third.find('a')
    url= lien.get('href')
    home_link_categorie.append(url)


categorie_name = []
list_sous_categ = []
for cat in home_link_categorie:
    contenu_de_ma_cat, code = get_page_content(cat)
    soup = BeautifulSoup(contenu_de_ma_cat, features="html.parser")
    nom_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    nom_cat.find("a").extract()
    nom_cat = nom_cat.getText()
    nom_cat = nom_cat.replace("> ", "")
    nom_cat = nom_cat.strip()
    categorie_name.append(nom_cat)

    liste_halfs = soup.find_all(class_="w3-half")
    for half in liste_halfs:
        lien = half.find('a')
        if lien:
            url= lien.get('href')
            list_sous_categ.append(url)
    
    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        if lien:
            url= lien.get('href')
            list_sous_categ.append(url)
    # print(list_sous_categ)
    cas_details = []
    for sous_cat in list_sous_categ:
        next_link = "?"
        page_max = 99999
        i = 0
        while(i <= page_max):
            print(i)
            contenu_de_ma_cat, code = get_page_content(sous_cat+next_link)
            soup = BeautifulSoup(contenu_de_ma_cat, features="html.parser")
            cas = soup.select("div.w3-panel > div .w3-border-left.w3-border-top.w3-left-align")
            #print(cas)
            for c in cas:
                titre = c.find("h4").text
                p = c.select("p p")[1]
                tdata = p.text.split('\n')
                loc = tdata[0].replace("Location: ", "")
                type = tdata[1].replace("Type:", "")
                date = tdata[2].replace("Date / Time:", "")
                comments = tdata[3].replace("Further Comments:", "")
                cas_details.append({"title": titre, "location": loc, "type": type, "date": date, "comment": comments})
            
            next_links = []
            next_link_path = soup.select("div.w3-quarter > h5 > a")
            for link in next_link_path:
                href = link.get('href')
                next_links.append(href)
            page_max = int(next_links[-1][18:19])
            i += 1
            next_link = f'?pageNum_paradata={i}&totalRows_paradata=146'
# print(cas_details)


mycursor = mydb.cursor()

for cas_name in cas_details:
    sql = "INSERT INTO elements (title, location, type, date, further_comment) VALUES (%s, %s, %s, %s, %s)"
    val = (cas_name["title"], cas_name["location"], cas_name["type"], cas_name["date"], cas_name["comment"])
    mycursor.execute(sql, val)


mydb.commit()


for category_name in categorie_name:
    sql = "INSERT INTO category (name) VALUES (%s)"
    val = (category_name,)
    mycursor.execute(sql, val)

mydb.commit()

# mycursor.execute("SELECT * FROM elements")

# myresult = mycursor.fetchall()

# for x in myresult:
#   print(x)

print(mycursor.rowcount, "record inserted.")