drop DATABASE if exists fantome;
CREATE DATABASE IF NOT EXISTS fantome;
USE fantome;

CREATE TABLE elements (
  id INT PRIMARY KEY AUTO_INCREMENT ,
  title VARCHAR(255),
  location VARCHAR(255),
  date VARCHAR(255),
  type VARCHAR(255),
  further_comment TEXT,
  month INT,
  weather BOOLEAN
);

CREATE TABLE category (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  parent INT,
  cat_type ENUM('geographics', 'reports')
);

CREATE TABLE elem_cat (
  id_elem INT,
  id_cat INT,
  FOREIGN KEY (id_elem) REFERENCES elements(id),
  FOREIGN KEY (id_cat) REFERENCES category(id),
  PRIMARY KEY (id_elem, id_cat)
);

ALTER USER 'narayan23'@'localhost' IDENTIFIED WITH mysql_native_password BY 'coucou123';
GRANT ALL PRIVILEGES ON fantome.* TO 'narayan23'@'localhost';
