from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector
import pymysql

mydb = mysql.connector.Connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='fantome'
)

mycursor = mydb.cursor()


def get_page_content(url):
    return urlopen("https://www.paranormaldatabase.com"+url).read()

# get content home
html_home = get_page_content("/index.html")

# get category home
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)


for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()

    sql = "INSERT INTO category (name) VALUES (%s)"
    val = (titre_cat,)
    mycursor.execute(sql, val)
    mydb.commit()


    list_sous_cat = []

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for third in liste_halfs:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)


    for ss_cat in list_sous_cat:
        html_sous_cat = get_page_content(ss_cat)
        soup = BeautifulSoup(html_sous_cat, features="html.parser")
        # prendre plutot le titre h3 de la page des cas
        titre_ss_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
        titre_ss_cat.find("a").extract()
        titre_ss_cat.find("a").extract()
        titre_ss_cat = titre_ss_cat.getText()
        titre_ss_cat = titre_ss_cat.replace("> ", "")
        titre_ss_cat = titre_ss_cat.strip()

        sql = "INSERT INTO category (name) VALUES (%s)"
        val = (titre_ss_cat,)
        mycursor.execute(sql, val)
        mydb.commit()

        # refaire tout pour chaque page de la pagination
        qs = "?"
        while(qs):
            html_sous_cat = get_page_content(ss_cat+qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")

            list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")

            for cas in list_cas:
                # parser le cas

                # title varchar(255),
                titre_cas = cas.select_one("h4").get_text()

                # recup le fameux P
                p = cas.select_one("p:nth-last-child(1)")
                p_text = p.get_text().split("\n")
                # location varchar(255),
                p_text[0] = p_text[0].replace("Location: ", "")
                # type varchar(255),
                p_text[1] = p_text[1].replace("Type: ", "")
                # date varchar(255),
                p_text[2] = p_text[2].replace("Date / Time: ", "")
                # comments varchar(255),
                p_text[3] = p_text[3].replace("Further Comments: ", "")
                
                sql = "INSERT INTO elements (title, location, type, date, further_comment) VALUES (%s, %s, %s, %s, %s)"
                val = (titre_cas, p_text[0], p_text[1], p_text[2], p_text[3])
                mycursor.execute(sql, val)
                mydb.commit()

mycursor.close()
mydb.close()

print(mycursor.rowcount, "record inserted.")
